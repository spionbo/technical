/**
 * desc: 盒子
 * created by bo.peng 2018/11/25.
 * email:spion@qq.com
 */
import {Component, Vue, Prop} from 'vue-property-decorator';
import {Icon} from 'ant-design-vue';
import {Types} from '@/store/config/Types';
import {DomPropsDto} from '@/dto/config';
import {ActionsDto} from "@/dto/actions";
import {globalConfig} from "@/config";

interface State {
    // 是否全屏
    fullScreen: boolean;
    // 样式管理
    contentStyle: any;
    // 是否收缩
    minus: boolean;
    // 防止最小化重复提交
    minusEventOpen: boolean;
}
@Component
export default class Box extends Vue {
    // 标题
    @Prop(String) title: string;
    @Prop(Object) content: DomPropsDto;
    // box相关
    @Prop(Object) props: DomPropsDto;
    // state 设置
    state: State = {
        fullScreen: false,
        minus: true,
        contentStyle: {},
        minusEventOpen: true,
    };

    // 全屏
    fullScreen() {
        this.state.fullScreen = !this.state.fullScreen;
        const className = this.state.fullScreen ? "blur" : '';
        const actions: ActionsDto = {
            type: Types.GLOBAL_TYPES.APP,
	        payload: Object.assign({}, globalConfig.app, {
	            class: className,
            }),
        };
        this.$store.commit(Types.GLOBAL_CONFIG, actions);
    }

    // 最大化最小化
    minusEvent() {
        if (!this.state.minusEventOpen) {
            return false;
        }
        this.state.minusEventOpen = false;
        const content: any = this.$refs.content;
        if (!content.$height) {
            content.$height = content.offsetHeight;
        }
        const height = content.$height;
        this.state.minus = !this.state.minus;
        if (!this.state.minus) {
            this.state.contentStyle = {height: height + 'px'};
            setTimeout(() => {
                this.state.contentStyle = {height: '0', flex: 'none'};
            }, 60);
        } else {
            this.state.contentStyle = {height: '0px'};
            setTimeout(() => {
                this.state.contentStyle = {height: height + 'px'};
                setTimeout(() => {
                    this.state.contentStyle = {};
                }, 310);
            }, 60);
        }
        setTimeout(() => {
            this.state.minusEventOpen = true;
        }, 370);
    }

    private render() {
        const className = ' com-box ' +
            (this.state.fullScreen ? 'fixed' : '');
        const minus = this.state.minus ? 'plus-circle' : 'minus-circle';
        let contentProps = {};
        if (this.content && this.state.contentStyle) {
            contentProps = Object.assign({}, this.content, {
		        style: Object.assign({}, this.state.contentStyle, this.content.style || {} ),
	        });
        }
        return (<article class={className} {...this.props}>
            <div class="title">
                <div class="edit">
                    <Icon onClick={this.fullScreen}
                          type={this.state.fullScreen ? 'shrink' : 'arrows-alt'}
                          style={{cursor: 'pointer'}}/>
                    <Icon onClick={this.minusEvent}
                          type={minus}
                          theme="filled"
                          style={{marginLeft: '10px', cursor: 'pointer'}}/>
                </div>
                <div class="name">{this.title || this.$route.meta.name}</div>
            </div>
            <div class='content' ref="content" {...contentProps}>
                {this.$slots.default}
            </div>
        </article>);
    }
}