/**
 *  author : bo.peng
 *  createTime 2018-12-06 19:07
 *  description : 显示原代码
 */
import {Component, Vue} from 'vue-property-decorator';
@Component
export default class Code extends Vue {
	private render() {
		const config = {
			style: {
				textAlign: 'left',
				marginTop: '10px',
				padding: '10px 15px',
				border: '1px solid rgba(0,0,0,.3',
				backgroundColor: 'rgba(0,0,0,.05',
			},
		};
		return (<pre {...config}>{this.$slots.default}</pre>);
	}
}