/**
 *  author: bo.peng
 *  createTime: 2018-12-17 15:34
 *  description:
 */
import Layout from '@/views/Layout';
/*export default {
	path: '/other',
	name: '其它',
	meta: {
		name: '其它',
	},
	component: Layout,
	children: [
		{
			path: 'software-tool',
			name: '软件工具等',
			meta: {
				name: '软件工具等',
			},
			component: import('@/views/other/softwareTool'),
		},
	],
};*/

export default {
	path: '/project',
	name: '其它技术',
	meta: {
		name: '其它',
	},
	component: Layout,
	children: [
		{
			path: 'code',
			name: '编码规范',
			meta: {
				name: '编码规范',
			},
			component: () => import(
				'@/views/project/code'),
		},
	],
};