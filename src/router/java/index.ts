/**
 *  author: bo.peng
 *  createTime: 2018-12-10 15:07
 *  description:
 */
import Layout from "@/views/Layout";

export default {
	path: 'java',
	name: 'java',
	component: Layout,
	children: [
		{
			path: 'spring-boot',
			name: 'spring-boot',
			meta: {
				name: 'spring-boot',
			},
			component: Layout,
		},
		{
			path: 'spring-cloud',
			name: 'spring-cloud',
			meta: {
				name: 'spring-cloud',
			},
			component: Layout,
		},
		{
			path: 'rongx',
			name: '容错dsfcvcv',
			meta: {
				name: '容错',
			},
			component: Layout,
		},
		{
			path: 'rgond',
			name: '容断cvcvcv',
			meta: {
				name: '容断',
			},
			component: Layout,
		},
		{
			path: 'route',
			name: '路由werewr',
			meta: {
				name: '路由',
			},
			component: Layout,
		},
		{
			path: 'security',
			name: '安全hhjghjhg',
			meta: {
				name: '安全',
			},
			component: Layout,
		},
		{
			path: 'config',
			name: '配置dfgvbnbvn',
			meta: {
				name: '配置',
			},
			component: Layout,
		},
	],
};