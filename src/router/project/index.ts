/**
 *  author : bo.peng
 *  createTime 2018-11-29 15:51
 *  description :
 */
import Layout from '@/views/Layout';
import project from './project';

export default {
    path: '/project',
    name: '项目管理',
	meta: {
		name: '项目管理',
		icon: 'folder',
	},
	component: Layout,
    children: [
	    {
		    path: 'code',
		    name: '编码规范',
            meta: {
		      name: '编码规范',
            },
		    component: () => import(
			    '@/views/project/code'),
	    },
        {
            path: 'name',
            name: '命名规范',
	        meta: {
		        name: '命名规范',
	        },
            component: () => import(
                '@/views/project/name'),
        },
	    project,
        {
            path: 'test',
            name: '单元测试',
	        meta: {
		        name: '单元测试',
	        },
            component: () => import(
                '@/views/project/test'),
        },
        {
            path: 'pack',
            name: '打包',
	        meta: {
		        name: '打包',
	        },
            component: () => import(
                '@/views/project/pack'),
        },
        {
            path: 'deploy',
            name: '布署',
	        meta: {
		        name: '布署',
	        },
            component: () => import(
                '@/views/project/deploy'),
        },
        {
            path: 'version',
            name: '项目版本管理',
	        meta: {
		        name: '项目版本管理',
	        },
            component: () => import(
                '@/views/project/version'),
        },
    ],
};