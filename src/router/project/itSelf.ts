/**
 *  author: bo.peng
 *  createTime: 2018-12-12 11:19
 *  description:
 */
import Layout from '@/views/Layout';

export default {
	path: 'itSelf',
	name: '本网站项目',
	meta: {
		name: '本网站项目',
		icon: 'file',
	},
	component: Layout,
	children: [
		{
			path: 'structure',
			name: '项目结构',
			meta: {
				name: '项目结构',
			},
			component: () => import('@/views/project/project/itSelf/structure'),
		},
		{
			path: 'common',
			name: '共公组件方法等',
			meta: {
				name: '项目共公组件方法等',
			},
			component: () => import('@/views/project/project/itSelf/common'),
		},
		{
			path: 'namingSpecification',
			name: '项目流程图',
			meta: {
				name: '项目流程图',
			},
			component: () => import('@/views/project/name'),
		},
	],
};