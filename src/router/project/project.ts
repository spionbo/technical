/**
 *  author: bo.peng
 *  createTime: 2018-12-11 17:02
 *  description: 相关项目
 */
import Layout from '@/views/Layout';
import itSelf from "./itSelf";

export default {
	path: 'allProject',
	name: '所有项目',
	meta: {
		name: '所有项目总结',
		icon: 'appstore',
	},
	component: Layout,
	children: [
		// 本网站项目
		itSelf,
	],
};