/**
 *  author: bo.peng
 *  createTime: 2018-12-10 15:09
 *  description:
 */
import Layout from "@/views/Layout";
export default {
	path: 'nginx',
	name: 'nginx',
	component: Layout,
	children: [
		{
			path: 'yumin',
			name: '跨域',
			meta: {
				name: '跨域sdf',
			},
			component: Layout,
		},
		{
			path: 'asdf',
			name: '路由sdf',
			meta: {
				name: '路由',
			},
			component: Layout,
		},
		{
			path: 'cv',
			name: 'd安全',
			meta: {
				name: '安全sdf',
			},
			component: Layout,
		},
		{
			path: 'naerewme',
			name: '配置ds',
			meta: {
				name: '配置',
			},
			component: Layout,
		},
	],
};