/**
 *  author: bo.peng
 *  createTime: 2018-12-10 15:10
 *  description:
 */
import Layout from "@/views/Layout";
export default {
	path: 'python',
	name: 'python',
	component: Layout,
	children: [
		{
			path: 'scrapy',
			name: '跨域d',
			meta: {
				name: '跨域',
			},
			component: Layout,
		},
		{
			path: 'asdf',
			name: '路由d',
			meta: {
				name: '路由',
			},
			component: Layout,
		},
	],
};