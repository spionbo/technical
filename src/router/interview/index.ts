/**
 *  author : bo.peng
 *  createTime 2018-11-29 16:13
 *  description:
 */
import Layout from '@/views/Layout';
export default {
    path: 'name',
    name: '面试题',
    component: Layout,
    children: [
        {
            path: 'suanfa',
            name: '算法',
	        meta: {
		        name: '算法',
	        },
            component: Layout,
        },
        {
            path: 'out',
            name: '输出',
	        meta: {
		        name: '输出',
	        },
            component: Layout,
        },
	    {
		    path: 'namingSpecification',
		    name: '代码优化',
		    meta: {
			    name: '代码优化',
		    },
		    component: Layout,
	    },
	    {
		    path: 'security',
		    name: '安全',
		    meta: {
			    name: '安全',
		    },
		    component: Layout,
	    },
    ],
};