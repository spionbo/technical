/**
 *  author : bo.peng
 *  createTime 2018-11-29 15:59
 *  description : 打包相关
 */
import Layout from '../../../views/Layout';
export default {
    path: 'webpack',
    name: 'webpack',
    component: Layout,
    children: [
        {
            path: 'config',
            name: 'webpack配置',
	        meta: {
		        name: 'webpack配置',
	        },
            component: Layout,
        },
        {
            path: 'vue-cli',
            name: 'vue-cli',
	        meta: {
		        name: 'vue-cli',
	        },
            component: Layout,
        },
        {
            path: 'react-script',
            name: 'react-script',
	        meta: {
		        name: 'react-script',
	        },
            component: Layout,
        },
    ],
};