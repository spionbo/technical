/**
 *  author : bo.peng
 *  createTime 2018-11-29 16:12
 *  description :
 */
import Layout from '../../../views/Layout';
export default {
    path: 'react',
    name: 'react',
    component: Layout,
    children: [
        {
            path: 'yuanli',
            name: 'react原理',
	        meta: {
		        name: 'react原理',
	        },
            component: Layout,
        },
        {
            path: 'methos',
            name: 'react常用方法',
	        meta: {
		        name: '常用方法',
	        },
            component: Layout,
        },
    ],
};