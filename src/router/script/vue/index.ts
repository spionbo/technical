/**
 *  author : bo.peng
 *  createTime 2018-11-29 16:06
 *  description :
 */
import Layout from '../../../views/Layout';
export default {
    path: 'vue',
    name: 'vue',
    component: Layout,
    children: [
        {
            path: 'shuangxiang',
            name: '双向绑定',
	        meta: {
		        name: '双向绑定',
	        },
            component: Layout,
        },
        {
            path: 'array',
            name: '数组实现',
	        meta: {
		        name: '数组实现',
	        },
            component: Layout,
        },
        {
            path: 'shengmin',
            name: '生命周期及使用常景',
	        meta: {
		        name: '生命周期及使用常景',
	        },
            component: Layout,
        },
        {
	        path: 'shengmin',
	        name: '生命周期及景',
	        meta: {
		        name: '生命周期及使用常景',
	        },
	        component: Layout,
        },
    ],
};