/**
 *  author : bo.peng
 *  createTime 2018-12-10 14:57
 *  description :
 */
import Layout from "@/views/Layout";

export default {
	path: 'javascript',
	name: 'javascript',
	component: Layout,
	children: [
		{
			path: 'sys',
			name: '同步异步',
			meta: {
				name: '同步异步',
			},
			component: Layout,
		},
		{
			path: 'dui',
			name: '堆栈',
			meta: {
				name: '堆栈',
			},
			component: Layout,
		},
		{
			path: 'rongcuo',
			name: 'js容错',
			meta: {
				name: '容错',
			},
			component: Layout,
		},
		{
			path: 'gouchang',
			name: '构造涵数',
			meta: {
				name: '构造涵数',
			},
			component: Layout,
		},
		{
			path: 'class',
			name: '创建对像，继承',
			meta: {
				name: '创建对像，继承',
			},
			component: Layout,
		},
	],
};