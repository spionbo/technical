/**
 *  author : bo.peng
 *  createTime 2018-11-29 16:02
 *  description :
 */
import Layout from '@/views/Layout';
import webpack from "@/router/script/webpack";
import vue from "@/router/script/vue";
import react from "@/router/script/react";
import javascript from "@/router/script/javascript";
export default {
    path: 'frontEnd',
    name: '前端',
	meta: {
		name: '前端开发',
	},
    component: Layout,
    children: [
        // javascript
        javascript,
	    // webpack
	    webpack,
	    // vue
	    vue,
	    // react
	    react,
    ],
};