/**
 *  author : bo.peng
 *  createTime 2018-11-29 13:01
 *  description :
 */
import Vue from 'vue';
import Router from 'vue-router';
import Container from '@/views/Container';
import project from './project';
import browser from './browser';
import script from './script';
/*import nodejs from './nodejs';
import java from './java';
import nginx from './nginx';
import python from './python';*/
import interview from './interview';
import other from './other';

Vue.use(Router);
const routesConfig = {
	routes: [
		{
			path: '/',
			component: Container,
			meta: {
				name: '首页',
			},
			children: [
				// 项目管理
				project,
				// 浏览器
				browser,
				// 前端
				script,
				// nodejs
				// nodejs,
				// java
				// java,
				// nginx
				// nginx,
				// python
				// python,
				// 面试
				interview,
				// 其它
				other,
			],
		},
	],
};
let routeLink = {};
let setPath: any = (list: any) => {
	let updatePath: any = (item: any, path: any) => {
		if (item.path !== '/') {
			let paths = item.path.replace('/', '').split('/');
			let getPath: any = (obj: any, paths: any) => {
				if (paths.length) {
					let key: any = paths[0];
					if (!obj[key]) {
						obj[key] = {
							name: item.name,
							path: item.path.replace(/\/:\w+$/, ''),
						};
					}
					getPath(obj[key], paths.slice(1));
				}
			};
			getPath(routeLink, paths);
			getPath = null;
		}
		if (item.children) {
			item.children.forEach((child: any) => {
				if (!child.path.startsWith('/')) {
					child.path = path + '/' + child.path;
				}
				updatePath(child, child.path);
			});
		}
	};
	list.forEach((item: any) => {
		let path = (item.path === '/' ? '' : item.path);
		updatePath(item, path);
	});
	updatePath = null;
	return list;
};
const routes = setPath(routesConfig.routes);
setPath = null;
const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	...routesConfig,
});
export {
	// 初始配置
	routesConfig,
	// 可调用链接
	routeLink,
	// router配置好的 path 地址全称
	routes,
	// router对像
	router,
};