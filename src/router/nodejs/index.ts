/**
 *  author: bo.peng
 *  createTime: 2018-12-10 17:36
 *  description:
 */
import Layout from "@/views/Layout";
export default {
	path: 'nodejs',
	name: 'nodejs',
	component: Layout,
	children: [
		{
			path: 'scrapy',
			name: '跨s域1',
			meta: {
				name: '跨域',
			},
			component: Layout,
		},
		{
			path: 'asdf',
			name: '路d由',
			meta: {
				name: '路由',
			},
			component: Layout,
		},
	],
};