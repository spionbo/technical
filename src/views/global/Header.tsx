/**
 *  author : bo.peng
 *  createTime 2018-11-24 10:58
 *  description :
 */
import {Component, Vue} from 'vue-property-decorator';
import {Layout, Menu, List, Avatar} from 'ant-design-vue';
import {Types} from '@/store/menu/Types';
import {Getter} from 'vuex-class';

@Component
export default class Header extends Vue {
	@Getter menu: [];
	mounted() {
		this.$store.dispatch(Types.MENU);
	}
	change( item: any) {
		this.$store.commit(Types.SUB_MENU, item);
	}
	private render() {
		return (<Layout.Header class="header">
			<List class='logo'>
				<List.Item.Meta
					avatar={ <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />}
					title='没完没了'
					description="技术学习文档"
				/>
			</List>
			<Menu
				theme="light"
				class="menu"
				mode="horizontal"
				defaultSelectedKeys={['2']}
				style={{lineHeight: '64px'}}
			>
				{
					this.menu.length && this.menu.map((item: any, i: number) => {
						return <Menu.Item key={i} onClick={this.change.bind(this, item)}>{item.name}</Menu.Item>;
					})
				}
			</Menu>
		</Layout.Header>);
	}
}