/**
 *  author : bo.peng
 *  createTime 2018-11-24 10:56
 *  description : 左侧栏目
 */
import {Component, Vue, Watch } from 'vue-property-decorator';
import {Icon, Layout, Menu, Avatar, List} from 'ant-design-vue';
import {router} from "@/router";
import {Getter} from 'vuex-class';
import {Menu as Menus} from '@/domain/menu/Menu';
import GlobalDto from "@/dto/config/Global";
const { SubMenu } = Menu;
const { Sider } = Layout;
const Child = (h: any, item: any, i: number|string) => {
	let dom;
	if (item.children && item.children.length) { // 父类
		dom = (
			<SubMenu key={String(i)} title={
				<span>
					<Icon type={item.meta.icon || "compass"} theme="outlined" />
					{item.name}
				</span>}>
				{
					item.children.map((subItem: any, j: number) => Child(h, subItem, (i + '-' + j)))
				}
			</SubMenu>
		);
	} else {// 子类
		dom = (<Menu.Item key={String(i)} onClick={() => router.push(item.path)}>
			<Icon type={item.meta.icon || "compass"} theme="outlined" />
			{item.name}
		</Menu.Item>);
	}
	return dom;
};

@Component
export default class LeftMenu extends Vue {
    @Getter('menuChild') menu: Menus[];
	@Getter globalConfig: GlobalDto;
	private defaultSelectedKeys: string[] = ["0"];
	@Watch('menu')
	onMenuLoad(menu: any) {
		const self: any = this;
		let getPath: any = (item: any, i: number|string) => {
			if (item.path === self.$route.path) {
				self.$set(self, 'defaultSelectedKeys', [String(i)]);
			} else if (item.children && item.children.length) { // 父类
				item.children.map((subItem: any, j: number) => getPath(subItem, (i + '-' + j)));
			}
		};
		menu && menu.map((item: any, i: number ) => getPath(item, i));
		getPath = null;
	}
    private render(h: any) {
    	if (this.menu) {
		    return (
			    <Sider {...this.globalConfig.leftMenu}
			           defaultSelectedKeys={this.defaultSelectedKeys}>
				    <div class="title">
					    <Icon type="appstore" style={{marginRight: '10px'}}/>
					    项目文档
				    </div>
				    <Menu {...this.globalConfig.leftMenu.children}>
					    {
						    this.menu.map((item: any, i: number ) => Child(h, item, i))
					    }
				    </Menu>
			    </Sider>
		    );
	    } else {
    		return "loadding";
	    }
    }
}