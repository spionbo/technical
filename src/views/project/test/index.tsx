/**
 *  author : bo.peng
 *  createTime 2018-12-17 15:25
 *  description : 单元测试
 */
import {Component, Vue} from 'vue-property-decorator';

@Component
export default class Test extends Vue {
	private render() {
		return (<article>单元测试</article>);
	}
}