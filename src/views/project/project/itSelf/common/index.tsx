/**
 *  author : bo.peng
 *  createTime 2018-12-10 14:21
 *  description : 公共组件，方法等
 */
import {Component, Vue} from 'vue-property-decorator';
import {Table, Tabs, Collapse} from 'ant-design-vue';
import DomProps from "@/dto/config/DomProps";
import {ComBox} from "@/components";
import data, {CommonDataContent, CommonDataDto, CommonDataMethos, CommonDataTabsDto} from "../data/common";
const {TabPane} = Tabs;
const {Panel} = Collapse;

const tabsConfig: DomProps = {
	props: {
		tabPosition: 'left',
		defaultActiveKey: '0',
	},
};
// 说明
@Component
class Explain extends Vue {
	render() {
		const item: CommonDataDto = this.$vnode.data as CommonDataDto;
		return <div style={{
				marginBottom: '10px',
				padding: '10px',
				borderBottom: '1px solid #ddd',
				background: '#f8f8f8',
			}}>
				<div>
					引用方式：
					<span style={{color: '#ff0000'}}>{item.import}</span>
				</div>
				<div style={{
					paddingTop: '5px',
					color: '#999',
					lineHeight: '22px',
				}}>
					{item.explain}
				</div>
			</div>;
	}
}
// 参数和说明
@Component
class Arguments extends Vue {
	render() {
		const self: any = this;
		const contents: CommonDataContent[] = self.$vnode.data.arguments as CommonDataContent[];
		return (<div style={{
			marginBottom: '10px',
			paddingBottom: '10px',
			borderBottom: '1px solid #ddd',
		}}>
			{
				contents.map((item: CommonDataContent) => {
					return (<div style={{marginTop: '5px'}}>
						<h3>
							<span style={{fontWeight: 'normal'}}>参数：</span>
							{item.title}
						</h3>
						{item.content}
					</div>);
				})
			}
		</div>);
	}
}
// 折叠
@Component
class Panels extends Vue {
	render() {
		const self: any = this;
		const item: CommonDataContent[] = self.$vnode.data.children as CommonDataContent[];
		return (<Collapse defaultActiveKey={['0']} accordion>
			{
				item.map( (item: CommonDataMethos, i: number) => <Panel
					header={item.title}
				    key={String(i)}>
						<Arguments {...{arguments: item.arguments}}/>
						{item.content}
					</Panel>)
			}
		</Collapse>);
	}
}

@Component
export default class Index extends Vue {
	private render() {
		return (<ComBox>
			<Tabs defaultActiveKey="0">
				{
					data.map( (item: CommonDataTabsDto, i: number) => {
						return <TabPane tab={item.title} key={String(i)}>
							<Tabs {...tabsConfig}>
								{
									item.children.map((item: CommonDataDto, k: number) => {
										return <TabPane tab={item.title} key={String(k)}>
											<Explain {...item}/>
											<Panels {...{children: item.children}}/>
										</TabPane>;
									})
								}
							</Tabs>
						</TabPane>;
					})
				}
			</Tabs>
		</ComBox>);
	}
}