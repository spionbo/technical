/**
 *  author : bo.peng
 *  createTime 2018-12-12 12:51
 *  description : 项目结构
 */
import {Component, Vue} from 'vue-property-decorator';
import {ComBox} from "@/components";
import {Table, Tabs} from 'ant-design-vue';
import data from './data/structure';
import paginationConfig from "@/config/pagination";

@Component
export default class Structure extends Vue {
	private render() {
		const tableConfig = {
			props: {
				columns: [{
					title: '名称',
					dataIndex: 'name',
					key: 'name',
				}, {
					title: '说明',
					dataIndex: 'value',
					key: 'value',
					width: '12%',
				}, {
					title: '详情',
					dataIndex: 'else',
					width: '70%',
					key: 'else',
				}],
				rowKey() {
					return (Math.random() * new Date().getTime()).toString();
				},
				dataSource: data,
				pagination: Object.assign({}, paginationConfig.pagination, {
					pageSize: 999,
				}),
			},
		};
		return (<article>
			<ComBox>
				<Table {...tableConfig} />
			</ComBox>
		</article>);
	}
}