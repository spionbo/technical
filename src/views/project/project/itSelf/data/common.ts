/**
 *  author: bo.peng
 *  createTime: 2018-12-13 13:19
 *  description: 公共方法
 */
export interface CommonDataContent {
	title: string;
	content: string;
}
// 方法字断
export interface CommonDataMethos extends CommonDataContent {
	// 参数说明
	arguments?: CommonDataContent[];
}
export interface CommonDataDto {
	// 标题
	title: string;
	// 说明
	explain: string;
	// 引用方式
	import: string;
	// 说明
	children: CommonDataMethos[];
}
export interface CommonDataTabsDto {
	title: string;
	children: CommonDataDto[];
}
// 项目结构命名
const names: CommonDataDto[] = [
	{
		title: 'Util',
		explain: 'Util工具相关方法',
		import: 'import {Util} from @/utils;',
		children: [
			{
				title: 'Util.type(target: any)',
				arguments: [
					{
						title : "target",
						content: '目标类型',
					},
				],
				content: '返回相关类弄 [Object] object',
			},
		],
	},
	{
		title: 'Date',
		explain: '日期时间相关处理',
		import: '@/utils/Date',
		children: [
			{
				title: 'Util.type(target: any)',
				content: '说明',
			},
		],
	},
];

// 命名规范
const methos: CommonDataDto[] = [
	{
		title: 'Util',
		explain: 'Util工具相关方法',
		import: 'import {Util} from @/utils;',
		children: [
			{
				title: '获取相关类型：Util.type(target: any): string',
				arguments: [
					{
						title : "target",
						content: '目标类型',
					},
				],
				content: '返回相关类型 [Object] object',
			},
			{
				title: '获取对像类型：Util.isObject(target: any): boolean',
				arguments: [
					{
						title : "target",
						content: '目标类型',
					},
				],
				content: '返回Boolean是否是对像',
			},
			{
				title: '获取数组类型：Util.isArray(target: any): boolean',
				arguments: [
					{
						title : "target",
						content: '目标类型',
					},
				],
				content: '返回Boolean是否是数组',
			},
			{
				title: '深度克隆：Util.clone(target: any): any',
				arguments: [
					{
						title : "target",
						content: '目标类型为数组或对像',
					},
				],
				content: '深度克隆返回相关类型',
			},
			{
				title: '删除属性：Util.deleteProperty(target: any, key: PropertyKey): boolean',
				arguments: [
					{
						title : "target",
						content: '目标类型',
					},
					{
						title : "key",
						content: '要删除的字段',
					},
				],
				content: ' 一级删除，不删除子对像中的属性',
			},
			{
				title: '删除为空的属性：Util.deleteEmptyProperty(target: any): void',
				arguments: [
					{
						title : "target",
						content: '目标类型',
					},
				],
				content: '如果对像中属性值为空，则删除该属性',
			},
		],
	},
	{
		title: 'Date',
		explain: '日期时间相关处理',
		import: '@/utils/Date',
		children: [
			{
				title: 'Util.type(target: any)',
				content: '说明',
			},
		],
	},
];
const tabs: CommonDataTabsDto[] = [
	{
		title: '变量',
		children: names,
	},
	{
		title: '方法',
		children: methos,
	},
	{
		title: '公共组件',
		children: methos,
	},
];
export default tabs;