/**
 *  author: bo.peng
 *  createTime: 2018-12-12 13:19
 *  description:
 */
export default [
	{
		name: 'assets',
		value: '静态文件',
		else: '',
		children: [
			{
				name: 'images',
				value: '存放图片路经',
				else: '所有图片存放地址',
			},
		],
	},
	{
		name: 'components',
		value: '公共组件',
		else: '自定义公共组件，一搬以Com开头命名',
	},
	{
		name: 'config',
		value: '全局配置',
		else: '包括一些头部，栏目，table，分页等配置。',
	},
	{
		name: 'domain',
		value: 'class对像',
		else: '用于存放对像',
	},
	{
		name: 'dto',
		value: 'interface接口',
		else: '一些对像类型基本在这里读取',
	},
	{
		name: 'router',
		value: '路由相关配置',
		else: '路由配置，配置所有页面，链接，router对像等对外输出',
		children: [
			{
				name: 'browser',
				value: '浏览器路路由',
				else: '浏览器路路由',
			},
			{
				name: 'interview',
				value: '面试路由',
				else: '面试路由',
			},
			{
				name: 'java',
				value: 'java路由',
				else: 'java路由',
			},
			{
				name: 'nginx',
				value: 'nginx路由',
				else: 'nginx路由',
			},
			{
				name: 'nodejs',
				value: 'nodejs路由',
				else: 'nodejs路由',
			},
			{
				name: 'project',
				value: 'project路由',
				else: 'project路由',
			},
			{
				name: 'python',
				value: 'python路由',
				else: 'python路由',
			},
			{
				name: 'script',
				value: 'script路由',
				else: 'script路由',
			},
			{
				name: 'server',
				value: 'server路由',
				else: 'server路由',
			},
		],
	},
	{
		name: 'store',
		value: '数据状态管理',
		else: '数据状态管理',
		children: [
			{
				name: 'config',
				value: '常用配置管理',
				else: '头部，栏目，底部，内容等配置',
			},
			{
				name: 'menu',
				value: '栏目管理',
				else: '栏目管理',
			},
		],
	},
	{
		name: 'style',
		value: '样式',
		else: '一些样式及相关配置',
	},
	{
		name: 'utils',
		value: '工具',
		else: '工具',
	},
	{
		name: 'views',
		value: 'view层',
		else: '所有页面展示页',
		children: [
			{
				name: 'global',
				value: '所有共用组件',
				else: '共用组件，用于头部，栏目等',
			},
			{
				name: 'nodejs',
				value: 'nodejs',
				else: 'nodejs',
			},
			{
				name: 'project',
				value: '项目管理',
				else: '项目管理相关',
				children: [
					{
						name: 'code',
						value: '编码规范',
						else: '编码规范',
					},
					{
						name: 'name',
						value: '命名规范',
						else: '命名规范',
					},
					{
						name: 'project',
						value: '所有项目经验总结展示',
						else: '',
						children: [
							{
								name: 'itSelf',
								value: '当前网站项目文档',
								else: '当前网站相关方法，变量，结构，流程等说明',
								children: [
									{
										name: 'common',
										value: '公共方法，变量',
										else: '公共方法变量说明',
									},
									{
										name: 'structure',
										value: '项目结构',
										else: '项目结构',
									},
								],
							},
						],
					},
				],
			},
			{
				name: 'script',
				value: 'js相关技术',
				else: 'js相关技术说明，vue，react等',
				children: [
					{
						name: 'vue',
						value: 'vue相关总结',
						else: 'vue相关总结',
					},
				],
			},
		],
	},
];