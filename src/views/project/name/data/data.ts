/**
 *  author : bo.peng
 *  createTime 2018-12-07 10:37
 *  description :
 */
export interface NamesKeyValueDto {
	name: string;
	value: string;
}
export interface ContentDto {
	title: string;
	content: string;
}
const data: NamesKeyValueDto[] = [{
	name: '目录',
	value: '小驼峰',
}, {
	name: '组件',
	value: '大驼峰',
}, {
	name: 'class',
	value: '大驼峰',
},
	{
		name: '方法名',
		value: '动词+名词',
	},
	{
		name: '文件名',
		value: '名词+动词',
	},
	{
		name: '变量 & 方法',
		value: '均要注释',
	},
	{
		name: '默认缩进空格',
		value: '4',
	},
	{
		name: '函数',
		value: '一个函数控制在20行以内(一屏以内)',
	},
	{
		name: '注释',
		value: '行上注释',
	},
	{
		name: '引入',
		value: '尽量使用import ,不要用require',
	},
	{
		name: '复数',
		value: '名词+s',
	},
	{
		name: '自定义组件',
		value: 'Com命名开头',
	},
];
const data1: NamesKeyValueDto[] = [
	{
		name: '列表',
		value: 'List',
	},
	{
		name: '创建',
		value: 'Create',
	},
	{
		name: '添加',
		value: 'Add',
	},
	{
		name: '更新',
		value: 'Update',
	},
	{
		name: '详情',
		value: 'Detail',
	},
	{
		name: '弹出框组件',
		value: 'Dialog',
	},
	{
		name: 'tab切换组件',
		value: 'Tab(tab组件较多时文件夹加Tab后缀即可)',
	},
	{
		name: '编辑 & 添加 + 编辑',
		value: 'edit',
	},
	{
		name: '删除',
		value: 'delete',
	},
];
data.map((item: any, i: number) => item.key = i);
data1.map((item: any, i: number) => item.key = i);
const vueData: ContentDto[] = [
	{
		title: 'views 下的文件夹命名',
		content: `<div>
			由名词组成（好例子：car order cart；坏例子：greet good）<br>
			以小写开头（好例子: car；坏例子: Car）<br>
			尽量只选用单个单词，如必须用多个单词，则使用驼峰命名法（好例子: car order cart carInfo；坏例子: car_info car-info）
			</div>`,
	},
	{
		title: 'views 下的 *.vue 文件命名',
		content: `
			尽量是名词<br>
			大写开头，开头的单词就是所属模块名字（CarDetail、CarEdit、CarList）<br>
			名字至少两个单词（好例子: CarDetail；坏例子: Car）<br>
			常用结尾单词有（Detail、Edit、List、Info、Report）
		`,
	},
	{
		title: 'vue 方法放置顺序',
		content: `
			{<br>
				components: {},<br>
				filters: {},<br>
				props: {},<br>
				data: {},<br>
				computed:{},<br>
				watch:{},<br>
				beforeCreated: {},<br>
				created: {},<br>
				beforeMount: {},<br>
				mounted: {},<br>
				beforeDestroy: {},<br>
				update: {},<br>
				beforeRouteUpdate: {},<br>
				destroyed: {},<br>
				activated: {},<br>
				deactivated: {},<br>
				methods: {},<br>
			}`,
	},
	{
		title: 'method 自定义方法命名',
		content: `
			动宾短语结构（好例子：jumpPage、openCarInfoDialog；坏例子：go、nextPage、show、open、login）
			<br>远程获取数据的方法以 get、post 开头，以 data 结尾（好例子：getListData、postFormData；坏例子：takeData、confirmData、getList、postForm）
			<br>事件方法以 on 开头（onTypeChange、onUsernameInput）
			<br>尽量使用常用单词开头（set、get、open、close、jump）
			<br>驼峰命名（好例子: getListData；坏例子: get_list_data、getlistData
		`,
	},
	{
		title: 'data props 方法注意点',
		content: `
			使用 data 里的变量时请先在 data 里面初始化
			<br>props 定义属性，请使用type指定类型
			<br>props 改变父组件数据的时候，如果是基础类型，则使用 $emit 进行事件传递；如果是复杂类型（对象），则可以直接改
			<br>不命名多余数据，可以用对象包装的字段就不要独立定义成单独的字段
		`,
	},
	{
		title: 'vue-router 路由命名规则',
		content: `
			使用小写
			<br>使用名词，如：/user/info，/user/articles
			<br>尽量使用一个单词，如：/products；如果包含多个单词，请使用中划线，如：/item-list
		`,
	},
	{
		title: 'Vuex 命名规则',
		content: `
			state、getters和actions名称都使用小写开头，使用驼峰命名法
			<br>mutations名称全部使用大写，并将mutations名称定义成常量，放入一个单独的js文件进行统一存放
			<br>actions名称使用动宾短语结构，跟组件的methods命名方式一致
		`,
	},
];
export const Types = {
	// 以table形式展示
	table: Symbol('table'),
	// 折叠
	panel: Symbol('Panel'),
};
export default [
	{
		title: '项目结构命名',
		content: data,
		type: Types.table,
	},
	{
		title: '命名规范',
		content: data1,
		type: Types.table,
	},
	{
		title: 'Vue',
		type: Types.panel,
		content: vueData,
	},
	/*{
		title: 'React',
		type: Types.panel,
		content: vueData,
	},
	{
		title: 'nodejs',
		type: Types.panel,
		content: vueData,
	},
	{
		title: 'java',
		type: Types.panel,
		content: vueData,
	},
	{
		title: 'python',
		type: Types.panel,
		content: vueData,
	},*/
];