/**
 *  author : bo.peng
 *  createTime 2018-11-24 11:07
 *  description : 命名规范
 */
import {Component, Vue} from 'vue-property-decorator';
import {paginationConfig} from '../../../config';
import {ComBox, ComCode} from '../../../components';
import {Table, Tabs, Collapse} from 'ant-design-vue';
import table from "@/config/table";
import data, {ContentDto, Types} from "./data/data";
const {TabPane} = Tabs;
const {Panel} = Collapse;
const tabConfig = Object.assign({}, table, {
	props: Object.assign({}, table.props, {
		columns: [{
			title: '名称',
			dataIndex: 'name',
		}, {
			title: '说明',
			dataIndex: 'value',
		}],
		bordered: true,
		pagination: Object.assign({}, paginationConfig.pagination, {
			pageSize: 999,
		}),
	}),
});

// 折叠
@Component
class Panels extends Vue {
	render() {
		const self: any = this;
		const contents: ContentDto[] = self.$vnode.data.content as ContentDto[];
		return (<Collapse defaultActiveKey={['0']} accordion>
			{
				contents.map( (item: ContentDto, i: number) => <Panel
					header={item.title}
					key={String(i)}>
					<div domPropsInnerHTML={item.content}/>
				</Panel>)
			}
		</Collapse>);
	}
}

@Component
export default class NamingSpecification extends Vue {
	public render() {
		const box = {
			props: {
				title: this.$route.name,
				content: {
					style: {
						padding: '10px',
					},
				},
			},
		};
		const getContens = (item: any) => {
			switch (item.type) {
				case Types.table:
					return <Table {...tabConfig} dataSource={item.content}/>;
					break;
				case Types.panel:
					return <Panels {...{content: item.content}}/>;
					break;
			}
		};
		return (
			<section class="horizontal-box">
				<ComBox {...box}>
					<Tabs>
						{
							data.map((item: any, i: number) => {
								return(<TabPane tab={item.title} key={String(i)}>
									{
										getContens(item)
									}
								</TabPane>);
							})
						}
					</Tabs>
				</ComBox>
			</section>
		);
	}
}