/**
 *  author : bo.peng
 *  createTime 2018-12-17 15:28
 *  description : 项目版本管理
 */
import {Component, Vue} from 'vue-property-decorator';

@Component
export default class Version extends Vue {
	private render() {
		return (<article>项目版本管理</article>);
	}
}