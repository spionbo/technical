import paginationConfig from "@/config/pagination";

/**
 *  author : bo.peng
 *  createTime 2018-12-06 18:55
 *  description :
 */
const columns = [{
	title: '名称',
	dataIndex: 'name',
}, {
	title: '说明',
	dataIndex: 'value',
}];
const data = [{
	name: '目录',
	value: '小驼峰',
}, {
	name: '组件',
	value: '大驼峰',
}, {
	name: 'class',
	value: '大驼峰',
},
	{
		name: '方法名',
		value: '动词+名词',
	},
	{
		name: '文件名',
		value: '名词+动词',
	},
	{
		name: '变量 & 方法',
		value: '均要注释',
	},
	{
		name: '默认缩进空格',
		value: '4',
	},
	{
		name: '函数',
		value: '一个函数控制在20行以内(一屏以内)',
	},
	{
		name: '注释',
		value: '行上注释',
	},
	{
		name: '引入',
		value: '尽量使用import ,不要用require',
	},
	{
		name: '复数',
		value: '名词+s',
	},
	{
		name: '自定义组件',
		value: 'Com命名开头',
	},
];
const data1 = [
	{
		name: '列表',
		value: 'List',
	},
	{
		name: '创建',
		value: 'Create',
	},
	{
		name: '添加',
		value: 'Add',
	},
	{
		name: '更新',
		value: 'Update',
	},
	{
		name: '详情',
		value: 'Detail',
	},
	{
		name: '弹出框组件',
		value: 'Dialog',
	},
	{
		name: 'tab切换组件',
		value: 'Tab(tab组件较多时文件夹加Tab后缀即可)',
	},
	{
		name: '编辑 & 添加 + 编辑',
		value: 'edit',
	},
	{
		name: '删除',
		value: 'delete',
	},
];
data.map((item: any, i: number) => item.key = i);
data1.map((item: any, i: number) => item.key = i);
export default {
	panel: [
		{
			header: '文件格式规范',
			content: [
				{
					text: `由于存在开发过程中使用不同编辑器编写代码的情况，
				而不同编辑器对文件的默认设置有所不同，为了避免这种差异，
				我们推荐使用.editorconfig这个工具来统一文件格式，将前端的代码文件风格初步统一：文件统一使用UTF-8编码，
				缩进量使用2格缩进。参考以下的.editorconfig配置：`,
					code: `root = true

[*]
charset = utf-8
indent_style = space
indent_size = 4
end_of_line = lf
insert_final_newline = true
trim_trailing_whitespace = true

[*.md]
insert_final_newline = false
trim_trailing_whitespace = false`,
				},
			],
		},
		{
			header: '使用jsdoc规范来编写注释',
			content: [
				{
					text: `对于重要的、或可能要开放给其他人用的函数和模块，需要添加详细注释，
					请使用jsdoc的注释标准来进行编写。jsdoc文档请参考：http://usejsdoc.org`,
				},
			],
		},
		{
			header: '统一使用async/await处理异步流程',
			content: [
				{
					text: `请不要在任何情况下编写callback形式的异步流程处理代码。
					请将异步代码通过Promise进行封装，然后使用async/await语法进行调用。
					这一可以让代码更易于阅读和维护。`,
				},
			],
		},
		{
			header: 'API输入输出格式规范',
			content: [
				{
					text: `1、使用POST/PUT方式的API，在发送参数时全部使用JSON格式发送数据；
2、所有API的响应数据，也统一使用JSON格式，基本数据格式如下：`,
					code: `{
	"code": "error code here",
	"desc": "error description here",
	"data": "business data here"
}`,
				},
			],
		},
		{
			header: '避免在组件中操作DOM',
			content: [
				{
					text: `避免直接在组件代码中使用DOM API来操作DOM，如有这类需求，请尝试编写directive来解决问题`,
				},
			],
		},
		{
			header: '避免在组件中直接调用axios来发送请求，将请求封装在独立模块中',
			content: [
				{
					text: `避免在组件中直接引入axios库来请求后端API，请将所有的后端请求放入专门的请求模块，
					然后在组件中引用这些请求模块来进行后端调用。这样做的好处是：`,
					code: `
1、 后端API的统一集中管理，方便维护
2、 后端调用和业务逻辑最小耦合，方便代码测试
3、 如有替换请求库的需求（如替换掉axios），代价可以降到最低
					`,
				},
			],
		},
	],
};