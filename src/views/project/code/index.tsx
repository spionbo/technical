/**
 *  author : bo.peng
 *  createTime 2018-12-06 20:07
 *  description : 编码规范
 */
import {Component, Vue} from 'vue-property-decorator';
import {ComBox, ComCode} from '../../../components';
import {Collapse} from 'ant-design-vue';
import data from "./data/data";
const Panel = Collapse.Panel;

@Component
export default class Code extends Vue {
	private render() {
		const box = {
		};
		const collapseConfig = {
			props: {
				bordered: false,
				defaultActiveKey: ['1'],
			},
		};
		return (
			<section class="vertical-flex">
				<ComBox {...box}>
					<Collapse {...collapseConfig}>
						{
							data.panel.map((item: any, key: number) => {
								return (
									<Panel header={item.header} key={key}>
										{
											item.content.map((obj: any) => {
												return (<div>
													{obj.text}
													{
														obj.desc && <pre>{obj.desc}</pre>
													}
													{
														obj.code && <ComCode>{obj.code}</ComCode>
													}
												</div>);
											})
										}
									</Panel>
								);
							})
						}
					</Collapse>
				</ComBox>
			</section>
		);
	}
}