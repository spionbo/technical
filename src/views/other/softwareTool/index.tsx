/**
 *  author : bo.peng
 *  createTime 2018-12-17 15:59
 *  description : 软件、工具等
 */
import {Component, Vue} from 'vue-property-decorator';
import { List, Avatar } from 'ant-design-vue';

@Component
export default class SoftWareTool extends Vue {
	private render() {
		const data = [
			{
				title: 'Ant Design Title 1',
			},
			{
				title: 'Ant Design Title 2',
			},
			{
				title: 'Ant Design Title 3',
			},
			{
				title: 'Ant Design Title 4',
			},
		];
		return (<article>
			<List
				itemLayout="horizontal"
				dataSource={data}
				renderItem={ (item: any) => (
					<List.Item>
						<List.Item.Meta
							avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
							title={<a href="https://ant.design">{item.title}</a>}
							description="Ant Design, a design language for background applications, is refined by Ant UED Team"
						/>
					</List.Item>
				)}
			/>
		</article>);
	}
}