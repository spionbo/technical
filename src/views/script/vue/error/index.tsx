/**
 *  author : bo.peng
 *  createTime 2018-12-10 17:42
 *  description : 常见报错
 */
import {Component, Vue} from 'vue-property-decorator';

@Component
export default class Error extends Vue {
	private render() {
		const errors: any = [
			{
				title: 'Duplicate named routes definition',
			},
		];
		return (<div>常见报错</div>);
	}
}