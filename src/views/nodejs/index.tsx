/**
 *  author : bo.peng
 *  createTime 2018-12-11 12:59
 *  description : nodejs
 */
import {Component, Vue} from 'vue-property-decorator';

@Component
export default class Nodejs extends Vue {
	private render() {
		return (<div>
			node --inspect-brk
		</div>);
	}
}