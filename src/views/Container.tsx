/**
 *  author : bo.peng
 *  createTime 2018-11-23 13:05
 *  description : 盒子
 */
import {Component, Vue, Watch} from 'vue-property-decorator';
import {Layout, Menu, Icon} from 'ant-design-vue';
import Header from './global/Header';
import LeftMenu from './global/LeftMenu';
import {Breadcrumb} from '../components';
import {Getter} from 'vuex-class';
import {GlobalDto} from '@/dto/config';

const {Content} = Layout;

@Component
export default class Container extends Vue {
	@Getter globalConfig: GlobalDto;
	private render() {
		return (<article {...this.globalConfig.app}>
			<Layout>
				<Header {...this.globalConfig.header}/>
				<Layout {...this.globalConfig.layout}>
					<LeftMenu/>
					<Layout>
						<Breadcrumb/>
						<Content {...this.globalConfig.container}>
							<router-view/>
						</Content>
						{/*<Footer>footer</Footer>*/}
					</Layout>
				</Layout>
			</Layout>
		</article>);
	}
}