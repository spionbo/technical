/**
 * desc: 全局配置
 * created by bo.peng 2018-12-02.
 * email:spion@qq.com
 */
import {GlobalDto} from '@/dto/config';
const global: GlobalDto = {
    app: {
        class: 'main',
    },
    layout: {
      style: {
	      overflowY: 'auto',
      },
    },
	header: {
    	class: 'header',
		children: {
			class: 'menu',
			props: {
				theme: "light",
				mode: "horizontal",
				defaultSelectedKeys: ['1'],
			},
			style: {
				lineHeight: '63px',
			},
		},
	},
	leftMenu: {
    	props: {
    		width: 200,
	    },
		class: 'left-menu',
    	children: {
		    class: 'menu',
		    props: {
			    theme: "dark",
			    mode: "inline",
		    },
	    },
	},
    container: {
	    class: 'container',
    },
};
export default global;