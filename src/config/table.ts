/**
 * desc: table相关配置
 * created by bo.peng 2018/11/20.
 * email:spion@qq.com
 */
import pagination from './pagination';

export default {
	props: {},
    table: {
        size: 'default',
    },
    pagination,
};