/**
 * desc: 总配置
 * created by bo.peng 2018/11/20.
 * email:spion@qq.com
 */
import tableConfig from './table';
import paginationConfig from './pagination';
import globalConfig from './global';

export {
	// 全局配置
	globalConfig,
    // table配置
    tableConfig,
    // 分页配置
    paginationConfig,
};