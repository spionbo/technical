/**
 *  author : bo.peng
 *  createTime 2018-11-22 20:12
 *  description :
 */
import {VueClass} from 'vue-class-component/lib/declarations';
import {Vue} from 'vue/types/vue';
interface Window {
    Main: any;
}
declare const Main: any;
declare module 'ant-design-vue' {
    export let Button: VueClass<Vue>;
    export let Layout: any;
    export let Menu: any;
    export let Icon: any;
    export let Input: any;
    export let Breadcrumb: any;
    export let Table: any;
    export let Card: any;
    export let Row: any;
    export let Col: any;
    export let Tabs: any;
    export let Collapse: any;
    export let List: any;
    export let Avatar: any;
}