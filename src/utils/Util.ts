/**
 *  author : bo.peng
 *  createTime 2018-12-10 11:01
 *  description :
 */
export const Util = {
	type(target: any): string {
		return Object.prototype.toString.call(target);
	},
	isObject(target: any): boolean {
		return Util.type(target) === "[Object] object";
	},
	isArray(target: any): boolean {
		return Util.type(target) === "[Array] array";
	},
	// 深度克隆
	clone(target: any): any {
		let objClone: any = Array.isArray(target) ? [] : {};
		/*if(type === 'array'){
			for(var i = 0, len = data.length; i < len; i++){
				obj.push(deepClone(data[i]));
			}
		} else if(type === 'object'){
			for(var key in data){
				obj[key] = deepClone(data[key]);
			}
		}*/
		if (target && typeof target === "object") {
			for (let key in target) {
				if (target.hasOwnProperty(key)) {
					// 判断ojb子元素是否为对象，如果是，递归复制
					if (target[key] && typeof target[key] === "object") {
						objClone[key] = Util.clone(target[key]);
					} else {
						// 如果不是，简单复制
						objClone[key] = target[key];
					}
				}
			}
		}
		return objClone;
	},
	// 一级删除，不删除子对像中的属性
	deleteProperty(target: any, key: PropertyKey): boolean {
		if (Reflect.has(target, key)) {
			Reflect.deleteProperty(target, key);
			return true;
		}
		return false;
	},
	// 如果对像中属性值为空，则删除该属性
	deleteEmptyProperty(target: any): void {
		let key: PropertyKey;
		for (key in target) {
			if (!target[key] || target[key] === '') {
				Util.deleteProperty(target, key);
			}
		}
	},
};