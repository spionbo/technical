/**
 * desc:
 * created by bo.peng 2018-12-02.
 * email:spion@qq.com
 */
export const Types = {
    // 全局配置
    GLOBAL_CONFIG: "globalConfig",
    // global-types
    GLOBAL_TYPES: {
      APP: Symbol('app'),
      LAYOUT: Symbol('layout'),
      HEADER: Symbol('header'),
      LEFT_MENU: Symbol('leftMenu'),
      CONTAINER: Symbol('container'),
    },
};