/**
 * desc: 配置中心
 * created by bo.peng 2018/12/1.
 * email:spion@qq.com
 */
import {Types} from './Types';
import {GlobalDto} from '@/dto/config';
import {globalConfig} from '@/config';
import {ActionsDto} from '@/dto/actions';

export default {
	state: {},
	mutations: {
		// 全局dom配置
		[Types.GLOBAL_CONFIG](state: GlobalDto, actions: ActionsDto) {
			switch (actions.type) {
				// app配置相关
				case Types.GLOBAL_TYPES.APP:
					state.app = Object.assign({}, state.app, actions.payload);
					break;
				// layout配置相关
				case Types.GLOBAL_TYPES.LAYOUT:
					state.layout = Object.assign({}, state.layout, actions.payload);
					break;
				// header配置相关
				case Types.GLOBAL_TYPES.HEADER:
					state.layout = Object.assign({}, state.header, actions.payload);
					break;
				// leftMenu配置相关
				case Types.GLOBAL_TYPES.LEFT_MENU:
					state.layout = Object.assign({}, state.leftMenu, actions.payload);
					break;
				// container配置相关
				case Types.GLOBAL_TYPES.CONTAINER:
					state.container = Object.assign({}, state.container, actions.payload);
					break;
			}
		},
	},
	actions: {},
	getters: {
		globalConfig: (state: GlobalDto) => {
			if (!state) {
				return globalConfig;
			}
			return Object.assign({}, globalConfig, state);
		},
	},
};