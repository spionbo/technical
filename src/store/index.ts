/**
 *  author : bo.peng
 *  createTime 2018-11-23 12:43
 *  description :
 */
import Vue from 'vue';
import Vuex from 'vuex';
import menu from './menu';
import config from "./config";

Vue.use(Vuex);
export default new Vuex.Store({
    // state,
    mutations: {
    },
    actions: {},
    modules: {
        // 全局配置
        config,
        // 栏目菜单
        menu,
    },
    getters: {
    },
});
