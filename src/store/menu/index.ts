/**
 *  author : bo.peng
 *  createTime 2018-11-29 16:28
 *  description :
 */
import {Types} from '@/store/menu/Types';
import {routes} from "@/router";
import {MenuDto} from '@/dto/menu/MenuDto';

const State: MenuDto = {
    menu: [],
    children: [],
};
export default {
    state: State,
    mutations: {
        [Types.MENU](state: MenuDto, arr: []) {
            state.menu = arr;
        },
        // 添加子栏目
        [Types.SUB_MENU](state: MenuDto, menu: MenuDto) {
            state.children = menu.children;
        },
    },
    actions: {
        // 添加栏目
        [Types.MENU]({commit}: any) {
            commit(Types.MENU, routes[0].children);
            commit(Types.SUB_MENU, routes[0].children[0]);
        },
    },
    getters: {
        menu: (state: MenuDto) => {
            return state.menu;
        },
        menuChild: (state: MenuDto) => {
            return state.children;
        },
    },
};