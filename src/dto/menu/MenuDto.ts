/**
 * desc:
 * created by bo.peng 2018/11/29.
 * email:spion@qq.com
 */
import {Menu} from '@/domain/menu/Menu';

export interface MenuDto {
    menu: Menu[];
    children: Menu[];
}