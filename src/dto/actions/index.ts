/**
 *  author : bo.peng
 *  createTime 2018-12-08 11:05
 *  description : 用于vuex 对像
 */
interface Actions {
	type: symbol;
	payload: any;
}
export {
	 Actions as ActionsDto,
};