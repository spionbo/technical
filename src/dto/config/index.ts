/**
 * desc:
 * created by bo.peng 2018/12/1.
 * email:spion@qq.com
 */
import GlobalDto from './Global';
import DomPropsDto from './DomProps';
export {
    // dom配置
	DomPropsDto,
    // 全局配置
    GlobalDto,
};