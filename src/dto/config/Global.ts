/**
 * desc: 全局样式
 * created by bo.peng 2018/12/1.
 * email:spion@qq.com
 */
import {DomPropsDto} from "@/dto/config/index";

export default interface Global {
    // app配置
    app: DomPropsDto;
    // layout 配置
    layout: DomPropsDto;
    // header 配置
	header: DomPropsDto;
	// left menu配置
	leftMenu: DomPropsDto;
    // container 配置
	container: DomPropsDto;
}