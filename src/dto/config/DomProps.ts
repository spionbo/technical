/**
 *  author : bo.peng
 *  createTime 2018-12-07 12:56
 *  description :
 */
interface Props {
	style?: object;
	class?: string;
	props?: any;
}
export default interface DomProps extends Props {
	children?: Props;
}