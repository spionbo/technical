/**
 * desc:
 * created by bo.peng 2018/11/29.
 * email:spion@qq.com
 */
export class Menu {
    path: string;
    name: string;
    icon: string;
}